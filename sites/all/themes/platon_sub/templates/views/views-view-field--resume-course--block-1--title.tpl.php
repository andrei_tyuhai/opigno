<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<div class="content-top">
    <div class="title">
      <?php print $output; ?>
    </div>
    <div class="content-more default-visible">
        <a href="#"><?php print t('view') ?></a>
    </div>
    <div class="default-hidden">
      <?php
      $body = $view->render_field('body', $view->row_index);
      $body = str_replace('<br>', ' ', $body);
      $body = strip_tags($body);
      ?>
      <?php print views_trim_text(array('max_length' => 128,'ellipsis' => true,'word_boundary' => true,'html' => true), $body) ?>
    </div>
    <div class="default-hidden">
        <a href="#" class="close-btn"></a>
    </div>
</div>
