<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 29.09.2017
 * Time: 17:07
 */

/**
 * Implements hook_preprocess_page().
 */
function platon_sub_preprocess_page(&$vars) {
  if (isset($vars['og_context_navigation'])) {
    $vars['og_context_navigation'] .= '<li class="active"><a href="#" class="platon-og-context-view-tab platon-og-context-notebook-tab node-tab" title="Notebook"><span class="element-invisible">(active tab)</span></a></li>';
  }


  global $user;
  $user = user_load($user->uid);
  $vars['user_image'] = theme(
    'image_style', array(
      'style_name' => 'user_thumbnail_small',
      'path' => !empty($user->picture->uri) ? $user->picture->uri : variable_get('user_picture_default'),
      'attributes' => array(
        'class' => 'custom-user-picture',
      ),
    )
  );

}

/**
 * Alter login form
 */
function platon_sub_form_user_login_block_alter(&$form, &$form_state, $form_id) {
  $form['user_login_fieldset']['name']['#prefix'] = '<div class="description">' . t('login with your username or email') . '</div>';
}

/**
 * Implements hook_preprocess_node().
 * @param $variables
 */
function platon_sub_preprocess_node(&$vars) {
  if (!empty($vars['node']->type) && $vars['node']->type === 'class') {
    // Unset class image
    if (isset($vars['content']['fields'][0]['opigno_course_image'])) {
      unset($vars['content']['fields'][0]);
    }

    if (isset($vars['opigno_class_courses'])) {
      //drupal_add_library('system', 'drupal.collapse');
      $table_of_contents = array(
        '#type' => 'container',
        '#prefix' => '<div class="btgrid">',
        '#suffix' => '</div>',
        '#attributes' => array('class' => array('class-table-of-contents', 'row')),
      );

      // First sort courses by the order set at the class
      _opigno_custom_sort_class_courses($vars['node'], $vars['opigno_class_courses']);

      foreach ($vars['opigno_class_courses'] as $key => $course) {
        if ($course['access'] === true) {
          $course_lessons = opigno_quiz_app_course_lessons($course['target_id']);

          $table_of_contents[$course['target_id']] = array(
            '#type' => 'container',
            '#attributes' => array('class' => array('course-table-of-contents', 'col', 'col-sm-6', 'col-md-4', 'col-lg-3')),
            'field_course_image' => field_view_field('node', $course['entity'], 'opigno_course_image', array(
              'label'=>'hidden',
              'settings' => array(
                'image_style' => 'course_thumbnail_image',
              )
            )),
            'course_contents' => array(
              '#type' => 'container',
              '#attributes' => array('class' => array('course-contents')),
              'course_title' => array(
                '#prefix' => '<div class="course-title">',
                '#type' => 'markup',
                '#markup' => l($course['entity']->title, 'node/' . $course['entity']->nid),
                '#suffix' => '</div>',
              ),
            ),
          );

          if (isset($course_lessons[$course['target_id']]) && !empty($course_lessons[$course['target_id']])) {
            foreach ($course_lessons[$course['target_id']] as $lesson_nid => $vid) {
              $lesson = node_load($lesson_nid, $vid);
              $table_of_contents[$course['target_id']]['course_contents']['lesson_' . $lesson_nid] = array(
                '#type' => 'fieldset',
                '#title' => '<h2 class="lesson-title">' . t($lesson->title) . '</h2>',
                '#prefix' => '<ol class="list-wrapper">',
                '#suffix' => '</ol>',
                /*'#attributes' => array (
                  'class' => array(
                    'collapsible', 'collapsed'
                  )
                )*/
              );

              $questions = quiz_get_questions($lesson_nid, $vid);

              foreach ($questions as $question_num => $question) {
                $table_of_contents[$course['target_id']]['course_contents']['lesson_' . $lesson_nid]['question_' . $question->nid] = array(
                  '#type' => 'markup',
                  '#markup' => l($question->title, 'node/' . $lesson_nid . '/take', array('query' => array('slide' => (int)$question_num + 1))),
                  '#prefix' => '<li class="list-item">',
                  '#suffix' => '</li>',
                );
              }
            }
          }


        }
      }

      $vars['content']['fields'][]['tabe_of_contents'] = $table_of_contents;

    }
  }
}

function _opigno_custom_sort_class_courses($class, &$courses) {

  $courses_order = opigno_sort_group_courses($class);
  $opigno_class_courses_sorted = array();
  foreach ($courses_order as $course_id) {
    $key = array_search($course_id, array_column($courses, 'target_id'));
    if ($key !== false) {
      $opigno_class_courses_sorted[] = $courses[$key];
    }
  }
  $courses = $opigno_class_courses_sorted;
}