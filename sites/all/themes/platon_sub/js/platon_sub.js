(function ($) {
  Drupal.behaviors.platonSub = {
    attach: function (context, settings) {
      $('.view-my-trainings .content-top').matchHeight();
      $('.class-table-of-contents .course-contents').matchHeight();

      $(document).ready(function() {
        $('.course-table-of-contents .course-contents .list-wrapper fieldset legend .fieldset-title').click(function(e) {
          //$('.class-table-of-contents .course-contents').matchHeight();
        });
      });



      $('.view-resume-course .views-row .content-more a').click(function(e) {
        e.preventDefault();
        if (!$(this).closest('.views-row').hasClass('open')) {
          $(this).closest('.views-row').addClass('open');
        }
      });
      $('.view-resume-course .close-btn').click(function(e) {
        e.preventDefault();
        if ($(this).closest('.views-row').hasClass('open')) {
          $(this).closest('.views-row').removeClass('open');
        }
      });
    }
  };
}(jQuery));









