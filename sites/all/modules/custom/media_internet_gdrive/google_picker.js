// The Browser API key obtained from the Google API Console.
// Replace with your own Browser API key, or your own key.
var developerKey = 'AIzaSyBZtbxrDFnTaEqkAix7UXEQKYiA5oEb-8w';

// The Client ID obtained from the Google API Console. Replace with your own Client ID.
var clientId = "968721893140-du3tcd85rtmdq21f8e4k091hein9af97.apps.googleusercontent.com"

// Replace with your own project number from console.developers.google.com.
// See "Project number" under "IAM & Admin" > "Settings"
var appId = "968721893140";

// Scope to use to access user's Drive items.
var scope = ['https://www.googleapis.com/auth/drive'];

var pickerApiLoaded = false;
var oauthToken;

// Use the Google API Loader script to load the google.picker script.
function loadPicker() {
  gapi.load('auth', {'callback': onAuthApiLoad});
  gapi.load('picker', {'callback': onPickerApiLoad});
}

function onAuthApiLoad() {

  window.gapi.auth.authorize(
      {
        'client_id': clientId,
        'scope': scope,
        'immediate': false
      },
      handleAuthResult
  );
}

function onPickerApiLoad() {
  pickerApiLoaded = true;
  createPicker();
}

function handleAuthResult(authResult) {
  if (authResult && !authResult.error) {
    oauthToken = authResult.access_token;
    createPicker();
  }
}

// Create and render a Picker object for searching images.
function createPicker() {
  if (pickerApiLoaded && oauthToken) {
    var view_own = new google.picker.DocsView()
        .setIncludeFolders(true)
        .setOwnedByMe(true);
    //view.setMimeTypes("image/png,image/jpeg,image/jpg");
    var picker = new google.picker.PickerBuilder()
        .setSize(1024,650)
        //.enableFeature(google.picker.Feature.NAV_HIDDEN)
        //.enableFeature(google.picker.Feature.MINE_ONLY)
        //.enableFeature(google.picker.Feature.MULTISELECT_ENABLED)
        .setAppId(appId)
        .setOAuthToken(oauthToken)
        .addView(view_own)
        .addView(new google.picker.DocsUploadView())
        .setDeveloperKey(developerKey)
        .setCallback(pickerCallback)
        .build();
    picker.setVisible(true);
  }
}

// A simple callback implementation.
function pickerCallback(data) {
  if (data.action == google.picker.Action.PICKED) {
    //var fileId = data.docs[0].id;

    var name = data.docs[0].name;
    var embedUrl = data.docs[0].embedUrl;
    console.log(embedUrl);


    /** Remove company reference from embed url
      * https://drive.google.com/a/krengeltech.com/file/d/0B1WQF9N2YpRtMmZkOTEyRHQ3TVE
      * would become
      * https://drive.google.com/file/d/0B1WQF9N2YpRtMmZkOTEyRHQ3TVE
    **/
    /*var regexp = /\/\/drive.google.com\/(a\/.+?\/)/g;
    var match = regexp.exec(embedUrl);

    if (match && typeof(match[1]) !== 'undefined' ) {
      embedUrl = embedUrl.replace(match[1], '');
    }*/

    //var embedUrl = data.docs[0].url;

    document.getElementById('edit-embed-code').value = embedUrl;

  }
}