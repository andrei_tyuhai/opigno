/**
 * @file wysiwyg_plugins_ckeditor_config.js
 *
 * Custom configuration for CKEditor.
 */

/**
 * Set up custom configurations for the CKEditor editor.
 */
CKEDITOR.editorConfig = function(config) {

  config.autoEmbed_widget = 'embedSemantic';
  config.embed_provider = '//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}&api_key=86b9a1772a79d55c48e53b';
  //UPLOADCARE_PUBLIC_KEY = 'b8e2236dce2877b4bca5';
  config.enterMode = CKEDITOR.ENTER_P;
  config.shiftEnterMode = CKEDITOR.ENTER_BR;

};