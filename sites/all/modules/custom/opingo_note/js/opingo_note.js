(function ($, Drupal, window, undefined) {

  var $ = jQuery;
  Drupal.behaviors.opingo_note = {
    attach: function (context, settings) {
      $("#opingo_note_dialog_show, .platon-og-context-notebook-tab").click(function () {
        $("#opingo_note_dialog").dialog({
          show: {
            effect: 'fade',
            duration: 1000
          },
          hide: {
            effect: 'fade',
            duration: 500
          },
          title: 'Notebook',
          width: 700,
          height: 700,
          position: ['middle', 120],
        }).removeClass('hidden');
        $('#edit-field-note-body-und-0-format--2').val("filtered_html").change();
        return false;
      });

      $(".note-edit").click(function () {
        $("#opingo_note_dialog").dialog({
          show: {
            effect: 'fade',
            duration: 1000
          },
          hide: {
            effect: 'fade',
            duration: 500
          },
          title: 'Notebook',
          width: 700,
          height: 380,
          position: ['middle', 120],
        }).removeClass('hidden');
        $('#edit-field-note-body-und-0-format--2').val("filtered_html").change();
      });

      $(document).ready(function() {
        $('.view-id-notebook .opingo-note-content').each(function() {
          var full = $(this).children('.full').first().html();
          var excerpt = $(this).children('.excerpt').first().html();

          if (full === excerpt) {
            $(this).children('.show-more').hide();
          }
        });
      });

      // Configure/customize these variables.
      var showChar = 100;  // How many characters are shown by default
      var ellipsestext = "...";
      var moretext = "Show more >";
      var lesstext = "Show less";


      /*$('.opingo-note-wraper .opingo-note-more').each(function() {
          var content = $(this).html();

          if(content.length > showChar) {

              var c = content.substr(0, showChar);
              var h = content.substr(showChar, content.length - showChar);

              var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="opingo-note-morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="opingo-note-morelink">' + moretext + '</a></span>';

              $(this).html(html);
          }
      });*/

      $(".opingo-note-morelink").off('click').on('click', moreClick);

      function moreClick() {
        if ($(this).hasClass("less")) {
          $(this).removeClass("less");
          $(this).html(moretext);
        }
        else {
          $(this).addClass("less");
          $(this).html(lesstext);
        }
        //$(this).parent().prev().slideToggle();
        $(this).closest('.opingo-note-content').children('.excerpt').slideToggle();
        $(this).closest('.opingo-note-content').children('.full').slideToggle();
        return false;
      };




    }
  };
})(jQuery, Drupal, window);
