<?php

/**
 * Get notes for referring_url/current url and
 * return as list including edit/delete etc...
 *
 * @param $variables -> contains:
 *             $variables['referring_url'] = url for notes
 *             $variables['notes'] = an array of all notes for referring url
 */
function theme_opingo_note_list($variables) {
  $output = '';
  if ($variables['notes'] && !empty($variables['notes'])) {
    $notes_array = $variables['notes'];
    foreach ($notes_array as $item) {
      $note = opingo_note_load($item->snid);
      $title = $note->title;
      $body = $note->field_note_body['und'][0]['value'];
      $excerpt = views_trim_text(array('max_length' => 128,'ellipsis' => true,'word_boundary' => true,'html' => true), $body);
      $content = $body;
      if ($body != $excerpt) {
        $content = '<div class="excerpt">' . $excerpt . '</div>                
                 <div class="full" style="display:none;">' . $body . '</div>
                 <div class="opingo-note-more"><a href="" class="opingo-note-morelink">' . t('Show more >') . '</a></div>';

      }


      $date = format_date($note->created, variable_get('opingo_note_date_format', 'short'));
      $edit = l(t('edit'), 'opingo_note/' . $item->snid . '/edit', array('attributes' => array('class' => array('use-ajax'))));
      $delete = l(t('delete'), 'opingo_note/' . $item->snid . '/delete_confirm', array('attributes' => array('class' => array('use-ajax'))));
      $output .= '<div class="opingo-note-wraper" id="note-' . $item->snid . '">
        <div class="note note-' . $item->snid . '">
          <div class="note-title">' . $title . '</div>
          <div class="note-date">' . t('Added on: ') . $date . '</div>
          <div class="opingo-note-content clearfix">
            ' . $content . '     
          </div>
          <div class="note-dud clearfix">              
              <div class="note-delete" title="' . t('Delete note') . '">' . $delete . '</div>
              <div class="note-edit" title="' . t('Edit note') . '">' . $edit . '</div>
          </div>
        </div>
      </div>';
    }
  }

//  $output = '';
//  $notes = array();
//
//  $header = array();
//  $header[] = t('Title');
//  if (user_access('edit class opingo_note') || user_access('edit any opingo_note')) {
//    $header[] = t('Author');
//  }
//  $header[] = t('Edit');
//  $header[] = t('Delete');
//  $header[] = t('Date');
//
//  if (isset($variables['notes']) && intval($variables['notes']->rowCount())) {
//    $table['header'] = $header;
//
//    // build rows for list
//    foreach ($variables['notes'] as $key => $opingo_note) {
//      if (_opingo_note_view_access($opingo_note)) {
//        $note_class = 'note-' . $opingo_note->snid;
//        $account = user_load($opingo_note->uid);
//        if (user_access('edit class opingo_note') || user_access('edit any opingo_note')) {
//          $notes[$opingo_note->snid] = array(
//            'data' => array(
//              'note' => array(
//                'data' => nl2br(filter_xss($opingo_note->title)),
//                'class' => $note_class,
//              ),
//              'author' => array(
//                'data' => l(t($account->name), 'user/' . $opingo_note->uid),
//                'class' => $note_class,
//              ),
//              'edit' => array(
//                'data' => l(t('edit'), 'opingo_note/' . $opingo_note->snid . '/edit', array('attributes' => array('class' => array('use-ajax')))),
//                'class' => $note_class,
//              ),
//              'delete' => array(
//                'data' => l(t('delete'), 'opingo_note/' . $opingo_note->snid . '/delete_confirm', array('attributes' => array('class' => array('use-ajax')))),
//                'class' => $note_class,
//              ),
//              'created' => array(
//                'data' => format_date($opingo_note->created, variable_get('opingo_note_date_format', 'short')),
//                'class' => $note_class,
//              ),
//            ),
//            'id' => 'note-' . $opingo_note->snid,
//          );
//        }
//        else {
//          $notes[$opingo_note->snid] = array(
//            'data' => array(
//              'note' => array(
//                'data' => nl2br(filter_xss($opingo_note->title)),
//                'class' => $note_class,
//              ),
//              'edit' => array(
//                'data' => l(t('edit'), 'opingo_note/' . $opingo_note->snid . '/edit', array('attributes' => array('class' => array('use-ajax')))),
//                'class' => $note_class,
//              ),
//              'delete' => array(
//                'data' => l(t('delete'), 'opingo_note/' . $opingo_note->snid . '/delete_confirm', array('attributes' => array('class' => array('use-ajax')))),
//                'class' => $note_class,
//              ),
//              'created' => array(
//                'data' => format_date($opingo_note->created, variable_get('opingo_note_date_format', 'short')),
//                'class' => $note_class,
//              ),
//            ),
//            'id' => 'note-' . $opingo_note->snid,
//          );
//        }
//
//        // If user doesn't have access, don't show edit link.
//        if (!_opingo_note_edit_access($opingo_note)) {
//          $notes[$opingo_note->snid]['data']['edit'] = NULL;
//        }
//
//        // If user doesn't have access don't show delete link.
//        if (!_opingo_note_delete_access($opingo_note)) {
//          $notes[$opingo_note->snid]['data']['delete'] = NULL;
//        }
//      }
//    }
//
//    $table['rows'] = $notes;
//
//    $output = theme('table', $table);
//  }

  return $output;
}

/**
 * Return confirmation links for deletion
 *
 * @param $variables
 */
function theme_opingo_note_confirm($variables) {
  $opingo_note = $variables['opingo_note'];
  $token = drupal_get_token($opingo_note->snid);

  $output = '';
  $output .= '<div class="opingo-note-wraper opingo-note-delete" id="note-' . $opingo_note->snid . '">';
  $output .= 'Are you sure?</br>';
  $output .= l(t('Delete'), 'opingo_note/' . $opingo_note->snid . '/delete', array(
    'attributes' => array('class' => array('use-ajax')),
    'query' => array('token' => $token),
  ));
  $output .= '&nbsp;&nbsp;&nbsp;&nbsp;';
  $output .= l(t('Cancel'), 'opingo_note/' . $opingo_note->snid . '/cancel', array('attributes' => array('class' => array('use-ajax'))));
  $output .= '</div>';

  return $output;
}
