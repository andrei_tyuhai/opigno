<?php

/**
 * @file
 * The controller class for the opingo_note entity
 */

class opingo_noteController extends DrupalDefaultEntityController {
  public function save($opingo_note) {
    try {
      drupal_write_record('opingo_note', $opingo_note);
      field_attach_insert('opingo_note', $opingo_note);
      module_invoke_all('entity_insert', $opingo_note, 'opingo_note');
      return $opingo_note;
    }
    catch (Exception $e) {
      watchdog_exception('opingo_note', $e);
      throw $e;
    }
  }

  public function update($opingo_note) {
    try {
      db_update('opingo_note')
        ->fields(array(
          'title' => $opingo_note->title,
          'updated' => $opingo_note->updated,
        ))
        ->condition('snid', $opingo_note->snid)
        ->execute();
      field_attach_update('opingo_note', $opingo_note);
      module_invoke_all('entity_update', $opingo_note, 'opingo_note');
      return $opingo_note;
    }
    catch (Exception $e) {
      watchdog_exception('opingo_note', $e);
      throw $e;
    }
  }

  public function delete($opingo_note) {
    try {
      db_delete('opingo_note')
        ->condition('snid', $opingo_note->snid)
        ->execute();
      field_attach_delete('opingo_note', $opingo_note);
      module_invoke_all('entity_delete', $opingo_note, 'opingo_note');
    }
    catch (Exception $e) {
      watchdog_exception('opingo_note', $e);
      throw $e;
    }
  }
}
