<?php

/**
 * Implements hook_views_data().
 */
function opingo_note_views_data() {
  $data['opingo_note']['table']['group'] = t('opingo_note Table');

  $data['opingo_note']['table']['base'] = array(
    'field' => 'snid',
    'title' => t('opingo_note'),
    'help' => t("opingo_note table contains opingo_note content for given paths."),
    'weight' => -10,
  );

  $data['opingo_note']['snid'] = array(
    'title' => t('opingo_note ID'),
    'help' => t('opingo_note ID'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['opingo_note']['title'] = array(
    'title' => t('Title'),
    'help' => t('opingo_note contents'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['opingo_note']['uid'] = array(
    'title' => t('opingo_note Author'),
    'help' => t('opingo_note author'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'relationship' => array(
      'base' => 'users', // The name of the table to join with.
      'base field' => 'uid', // The name of the field on the joined table.
      // 'field' => 'nid' -- see hook_views_data_alter(); not needed here.
      'handler' => 'views_handler_relationship',
      'label' => t('opingo_note Author'),
      'title' => t('opingo_note Author'),
    ),
  );

  $data['opingo_note']['url'] = array(
    'title' => t('URL'),
    'help' => t('URL opingo_note is saved on.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );


  $data['opingo_note']['created'] = array(
    'title' => t('Created time'),
    'help' => t('Created timestamp'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['opingo_note']['updated'] = array(
    'title' => t('Updated timed'),
    'help' => t('Updated timestamp'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}
