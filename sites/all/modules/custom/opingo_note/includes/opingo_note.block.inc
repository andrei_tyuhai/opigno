<?php
/**
 * @file
 * Defines the opingo_note block contents
 */

/**
 * Return the opingo_note block's add form
 * @return string
 */
function opingo_note_block_contents() {
  $output = '';

  $opingo_note = (object) array(
    'snid' => '',
    'type' => 'opingo_note',
    'note' => '',
    'url' => '',
    'uid' => '',
  );
  drupal_add_library('system', 'ui.dialog');
  $path = drupal_get_path('module', 'opingo_note');
  drupal_add_js($path . '/js/opingo_note.js');
  $form = drupal_get_form('opingo_note_add_form', $opingo_note);
  $output .= '<div id="opingo_note_dialog" class="hidden opingo_note_dialog"><div id="add-edit-form">' . drupal_render($form) . '</div></div>';
  return $output;
}
