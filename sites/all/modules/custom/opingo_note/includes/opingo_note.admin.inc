<?php
/**
 * @file
 * Defines the admin page for the opingo_note module
 */

/**
 * Admin page callback function.
 *
 * @return string
 */
function opingo_note_info() {
  $notes_per_page = 20;
  $header = array(
    array(
      'data' => 'snid',
      'field' => 's.snid',
    ),
    array(
      'data' => 'Title',
      'field' => 's.title',
    ),
    array(
      'data' => 'URL',
      'field' => 's.url',
    ),
    array(
      'data' => 'User',
      'field' => 's.uid',
    ),
    array(
      'data' => 'Created',
      'field' => 's.created',
      'sort' => 'desc',
    ),
    array(
      'data' => 'Edit',
      'field' => 's.snid',
    ),
  );
  $query = db_select('opingo_note', 's')->extend('PagerDefault')->extend('TableSort');
  $query
    ->fields('s')
    ->orderByHeader($header)
    ->limit($notes_per_page);

  $result = $query->execute();

  $notes = array();
  $destination = drupal_get_destination();
  $options = [
    'query' => [
      'destination' => $destination['destination'],
    ]
  ];
  foreach ($result as $key => $row) {
    $user = user_load($row->uid, FALSE);
    $user_name = $user->uid == 0 ? 'Anon' :  l($user->name, 'user/' . $user->uid);
    $notes[] = array(
      'data' => array(
        'snid' => l($row->snid, 'opingo_note/' . $row->snid),
        'title' => filter_xss($row->title),
        'url' => l($row->url, $row->url),
        'user' => $user_name,
        'created' => date('n/j/Y - G:i', $row->created),
        'edit' => l(t('Edit'), 'opingo_note/edit/' . $row->snid, $options),
      )
    );
  }

  $build['opingo_notes_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $notes,
    '#prefix' => t('This table lists all existing opingo_notes and their respective data.'),
    '#empty' => t('There are currently no opingo_notes.')
  );
  $build['opingo_notes_pager'] = array('#theme' => 'pager');

  return $build;
}

/**
 * opingo_note configuration options
 */
function opingo_note_config() {
  $form = array();
  $date_formats = system_get_date_types();

  foreach ($date_formats as $format) {
    $options[$format['type']] = $format['title'];
  }

  $form['opingo_note_date_format'] = array(
    '#type' => 'select',
    '#title' => t('opingo_note Date Format'),
    '#options' => $options,
    '#default_value' => variable_get('opingo_note_date_format', 'short'),
    '#description' => t('Select a date format that will apply to opingo_notes across the site.'),
  );

  return system_settings_form($form);
}
