<?php
/**
 * @file
 * krengeltech_feedback.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function krengeltech_feedback_defaultconfig_features() {
  return array(
    'krengeltech_feedback' => array(
      'strongarm' => 'strongarm',
    ),
  );
}

/**
 * Implements hook_defaultconfig_strongarm().
 */
function krengeltech_feedback_defaultconfig_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'webform_feedback';
  $strongarm->value = '116';
  $export['webform_feedback'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'webform_feedback_position';
  $strongarm->value = 'right';
  $export['webform_feedback_position'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'webform_feedback_text';
  $strongarm->value = 'Feedback';
  $export['webform_feedback_text'] = $strongarm;

  return $export;
}
