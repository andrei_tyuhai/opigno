<?php
/**
 * @file
 * krengeltech_feedback.features.webform.inc
 */

/**
 * Implements hook_webform_defaults().
 */
function krengeltech_feedback_webform_defaults() {
$webforms = array();
  $webforms['feedback'] = array(
  'title' => 'Feedback',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 1,
  'sticky' => 0,
  'type' => 'webform',
  'language' => 'und',
  'tnid' => 0,
  'translate' => 0,
  'path' => array(
    'pathauto' => 1,
  ),
  'webform' => array(
    'next_serial' => 7,
    'confirmation' => '<h2>Thanks for your feedback!</h2>
',
    'confirmation_format' => 'html',
    'redirect_url' => '<none>',
    'status' => 1,
    'block' => 1,
    'allow_draft' => 0,
    'auto_save' => 1,
    'submit_notice' => 0,
    'confidential' => 0,
    'submit_text' => '',
    'submit_limit' => -1,
    'submit_interval' => -1,
    'total_submit_limit' => -1,
    'total_submit_interval' => -1,
    'progressbar_bar' => 0,
    'progressbar_page_number' => 0,
    'progressbar_percent' => 0,
    'progressbar_pagebreak_labels' => 0,
    'progressbar_include_confirmation' => 0,
    'progressbar_label_first' => 'Start',
    'progressbar_label_confirmation' => 'Complete',
    'preview' => 0,
    'preview_next_button_label' => '',
    'preview_prev_button_label' => '',
    'preview_title' => '',
    'preview_message' => '',
    'preview_message_format' => 'html',
    'preview_excluded_components' => array(),
    'machine_name' => 'feedback',
    'record_exists' => TRUE,
    'roles' => array(
      0 => 2,
    ),
    'emails' => array(),
    'components' => array(
      'feedback__title' => array(
        'form_key' => 'title',
        'name' => 'Title',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'description_above' => 0,
          'private' => 0,
          'wrapper_classes' => '',
          'css_classes' => '',
          'width' => '',
          'maxlength' => '',
          'minlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'disabled' => 0,
          'unique' => 0,
          'description' => '',
          'placeholder' => '',
          'attributes' => array(),
          'analysis' => FALSE,
        ),
        'required' => 1,
        'weight' => 0,
        'machine_name' => 'feedback__title',
        'page_num' => 1,
      ),
      'feedback__message' => array(
        'form_key' => 'message',
        'name' => 'Message',
        'type' => 'html_textarea',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'description_above' => 0,
          'private' => 0,
          'wrapper_classes' => '',
          'css_classes' => '',
          'show_format_fieldset' => 0,
          'format' => 'filtered_html',
          'cols' => '',
          'rows' => '',
          'resizable' => 1,
          'disabled' => 0,
          'description' => '',
          'attributes' => array(),
          'show_tips' => 1,
          'show_link' => 1,
        ),
        'required' => 1,
        'weight' => 1,
        'machine_name' => 'feedback__message',
        'mandatory' => 0,
        'page_num' => 1,
      ),
    ),
    'conditionals' => array(),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'b:0;',
  'print_pdf_size' => NULL,
  'print_pdf_orientation' => NULL,
  'webform_features_author' => 'admin',
  'webform_features_revision_author' => 'admin',
);
return $webforms;
}
