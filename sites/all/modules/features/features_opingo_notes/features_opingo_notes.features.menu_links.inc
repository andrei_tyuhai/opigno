<?php
/**
 * @file
 * features_opingo_notes.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function features_opingo_notes_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_notes:<nolink>.
  $menu_links['main-menu_notes:<nolink>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<nolink>',
    'router_path' => '<nolink>',
    'link_title' => 'Notes',
    'options' => array(
      'attributes' => array(
        'id' => 'opingo_note_dialog_show',
        'class' => array(
          0 => 'ctools-use-modal',
        ),
      ),
      'item_attributes' => array(
        'id' => 'opingo_note_dialog_show',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_notes:<nolink>',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -41,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Notes');

  return $menu_links;
}
