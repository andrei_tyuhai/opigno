<?php
/**
 * @file
 * features_opingo_notes.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function features_opingo_notes_defaultconfig_features() {
  return array(
    'features_opingo_notes' => array(
      'field_default_fields' => 'field_default_fields',
    ),
  );
}

/**
 * Implements hook_defaultconfig_field_default_fields().
 */
function features_opingo_notes_defaultconfig_field_default_fields() {
  $fields = array();

  // Exported field: 'opingo_note-opingo_note-field_note_node_ref'.
  $fields['opingo_note-opingo_note-field_note_node_ref'] = array(
    'field_config' => array(
      'active' => 1,
      'cardinality' => 1,
      'deleted' => 0,
      'entity_types' => array(),
      'field_name' => 'field_note_node_ref',
      'foreign keys' => array(
        'node' => array(
          'columns' => array(
            'target_id' => 'nid',
          ),
          'table' => 'node',
        ),
      ),
      'indexes' => array(
        'target_id' => array(
          0 => 'target_id',
        ),
      ),
      'locked' => 0,
      'module' => 'entityreference',
      'settings' => array(
        'handler' => 'base',
        'handler_settings' => array(
          'behaviors' => array(
            'views-select-list' => array(
              'status' => 0,
            ),
          ),
          'sort' => array(
            'type' => 'none',
          ),
          'target_bundles' => array(
            'class' => 'class',
            'course' => 'course',
            'quiz' => 'quiz',
          ),
        ),
        'target_type' => 'node',
      ),
      'translatable' => 0,
      'type' => 'entityreference',
    ),
    'field_instance' => array(
      'bundle' => 'opingo_note',
      'default_value' => NULL,
      'default_value_function' => '',
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'entityreference',
          'settings' => array(
            'bypass_access' => FALSE,
            'link' => FALSE,
          ),
          'type' => 'entityreference_label',
          'weight' => 2,
        ),
        'full' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'opingo_note',
      'field_name' => 'field_note_node_ref',
      'label' => 'Node ref',
      'required' => 0,
      'settings' => array(
        'behaviors' => array(
          'prepopulate' => array(
            'status' => 0,
          ),
        ),
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_select',
        'weight' => 3,
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Node ref');

  return $fields;
}
