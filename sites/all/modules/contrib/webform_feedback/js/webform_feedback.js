(function ($) {
  Drupal.behaviors.webform_feedback = {
    attach: function (context, settings) {
        $('#block-webform-client-block-' + Drupal.settings.webform_feedback.key).addClass('webform-feedback-block');
        $('#webform-client-form-' + Drupal.settings.webform_feedback.key).addClass('webform-feedback-form');
        $('form.webform-feedback-form:not(.filter) :input:visible:enabled:first').focus()
        $('#webform-feedback a').click(function() {
          $(Drupal.settings.webform_feedback.inert).prop('inert', 'inert');
          $('#mask-bg').show();
          CLOSE=0;
        });
        function closeWindow() {
          if (CLOSE == 0) {
            $('#mask-bg').hide();
            $('#webform-feedback-block').slideToggle();
            $('#formclose').slideToggle(750);
            $(Drupal.settings.webform_feedback.inert).removeAttr('inert');
            CLOSE = 1;
            return false;
          }
        }
        $('a.webform-feedback-close, #mask-bg:not(#webform-feedback-block)').click(function() {
          closeWindow();
        });
        $(document).keyup(function(e) {
          if ( (e.keyCode == 27) && (CLOSE == 0) ) {
            closeWindow();
          }
        });
        if($(window).width() < 820) {
          $('#webform-feedback-block').addClass('small');
        }
        if($('#ajax-loader').length == 0)
          $('#webform-feedback-block  .form-actions input[type=submit]').after('<div id="ajax-loader"></div>');
        $("form.webform-feedback-form").submit(function (e) {
          $('#webform-feedback-block input[type=submit]').hide();
          $('#webform-feedback-block #ajax-loader').show();
          var formId = this.id;  // "this" is a reference to the submitted form
        });

    }
  };
}(jQuery));
