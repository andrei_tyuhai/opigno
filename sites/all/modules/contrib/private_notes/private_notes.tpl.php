<?php
/**
 * @file
 * Theme implementation for private notes.
 *
 * Available variables:
 * - $added by: Note author
 * - $created_on: Date on which note was created
 * - $note : Note text.
 */
?>
<div class="PrivateNotes">
  <div class="title">
    <?php print $added_by; ?> On <?php print $created_on; ?>
  </div>
  <div class="note">
    <?php print $note ?>
  </div>
  <hr class="sep">
</div>
