CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------
Private notes is used to create node specific private notes on content pages. 
These notes are private and it is visible to only those users who have given
user roles with permission to add/view notes.

* For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/mepushpendra/2758273

REQUIREMENTS
-----------
Drupal 7

Installation
------------
* Install as you would normally install a contributed Drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

CONFIGURATION
-------------
* Go to Structure >> Blocks >> Add Notes >> Click on Configure link.
* Select content types on which you want to show the private notes.
* Select region in which you want this block to appear.
* Visibility Settings >> Pages >> Only the listed pages >> enter 'node/*' and
 Save block.

TROUBLESHOOTING
---------------
* If the block does not display on Node view page, check the following:

   - Is user having permission to add & view note?
   - Is block configured to be shown in given region?
   - Is private notes allowed for the given content type?

MAINTAINERS
-----------
Current maintainers:
* Pushpendra Kumar(mepushpendra) - (https://www.drupal.org/u/mepushpendra)
